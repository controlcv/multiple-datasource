package com.zhjw.annotation;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by zhjw on 2023/3/6
 * <p>
 * 切换数据库注解
 * </p>
 *
 * @author zhjw
 * @date 2023/3/6
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface DS {

    /**
     * 数据库名称
     */
    String value() default "master";
}
