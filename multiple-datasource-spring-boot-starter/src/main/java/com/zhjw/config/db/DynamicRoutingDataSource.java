package com.zhjw.config.db;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * Created by zhjw on 2023/3/6
 * <p>
 * 实现Spring的AbstractRoutingDataSource接口，设置当前线程使用的数据源
 * </p>
 *
 * @author zhjw
 * @date 2023/3/6
 */
public class DynamicRoutingDataSource extends AbstractRoutingDataSource {

    @Override
    protected Object determineCurrentLookupKey() {
        return DataSourceContextHolderWrapper.getDB();
    }
}
