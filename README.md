# multiple-datasource

## 介绍

自定义注解**@DS**实现动态数据源切换且支持单体事务，并集成为springboot的starter启动项

## 软件架构

springboot + mybatis + java1.8

## 使用说明

1. 下载该项目
2. 将使用Maven指令将其安装到本地Maven仓库当中
3. 在需要切换数据源的Service类或Method中加上@DS注解
4. 在启动类上加上@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
5. 配置application.properties文件
6. 启动项目

## 实现思路

参考博客:
[多数据源切换(一)](https://blog.csdn.net/weixin_49604576/article/details/129447401)
[多数据源切换(二)](https://blog.csdn.net/weixin_49604576/article/details/129448068)
[多数据源切换(三)](https://blog.csdn.net/weixin_49604576/article/details/129448417)