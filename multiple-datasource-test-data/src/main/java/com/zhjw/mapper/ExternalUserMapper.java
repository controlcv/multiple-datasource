package com.zhjw.mapper;

import com.zhjw.entity.ExternalUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Entity com.zhjw.entity.ExternalUser
 */
@Mapper
public interface ExternalUserMapper {

    /**
     * 插入
     *
     * @param externalUser 插入实体
     * @return
     */
    void insert(ExternalUser externalUser);
}
