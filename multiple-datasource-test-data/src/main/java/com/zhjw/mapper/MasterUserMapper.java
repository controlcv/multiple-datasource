package com.zhjw.mapper;

import com.zhjw.entity.MasterUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Entity com.zhjw.entity.MasterUser
 */
@Mapper
public interface MasterUserMapper {

    /**
     * 插入
     *
     * @param masterUser 插入实体
     * @return
     */
    void insert(MasterUser masterUser);
}
