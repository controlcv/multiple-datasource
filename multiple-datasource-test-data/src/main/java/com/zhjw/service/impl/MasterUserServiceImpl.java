package com.zhjw.service.impl;

import com.zhjw.entity.MasterUser;
import com.zhjw.mapper.MasterUserMapper;
import com.zhjw.service.MasterUserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by zhjw on 2023/3/6
 * <p>
 *
 * </p>
 *
 * @author zhjw
 * @date 2023/3/6
 */
@Service
public class MasterUserServiceImpl implements MasterUserService {

    @Resource
    private MasterUserMapper masterUserMapper;

    /**
     * 插入
     *
     * @param masterUser 插入实体
     * @return
     */
    @Override
    public void insert(MasterUser masterUser) {
        masterUserMapper.insert(masterUser);
    }
}
