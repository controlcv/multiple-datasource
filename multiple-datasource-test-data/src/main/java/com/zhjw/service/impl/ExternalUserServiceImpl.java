package com.zhjw.service.impl;


import com.zhjw.annotation.DS;
import com.zhjw.entity.ExternalUser;
import com.zhjw.mapper.ExternalUserMapper;
import com.zhjw.service.ExternalUserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * Created by zhjw on 2023/3/6
 * <p>
 *
 * </p>
 *
 * @author zhjw
 * @date 2023/3/6
 */
@Service
public class ExternalUserServiceImpl implements ExternalUserService {
    @Resource
    private ExternalUserMapper externalUserMapper;

    /**
     * 插入
     *
     * @param externalUser 插入实体
     * @return
     */
    @DS("external")
    @Override
    @Transactional
    public void insert(ExternalUser externalUser) {
        externalUserMapper.insert(externalUser);
    }

    /**
     * 插入
     *
     * @param externalUser 插入实体
     * @return
     */
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRES_NEW)
    @DS("external")
    @Override
    public void unionTransactional(ExternalUser externalUser) {
        externalUserMapper.insert(externalUser);
    }
}
