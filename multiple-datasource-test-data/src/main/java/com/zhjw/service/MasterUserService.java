package com.zhjw.service;

import com.zhjw.entity.MasterUser;

/**
 * Created by zhjw on 2023/3/6
 * <p>
 *
 * </p>
 *
 * @author zhjw
 * @date 2023/3/6
 */
public interface MasterUserService {

    /**
     * 插入
     *
     * @param masterUser 插入实体
     * @return
     */
    void insert(MasterUser masterUser);
}
