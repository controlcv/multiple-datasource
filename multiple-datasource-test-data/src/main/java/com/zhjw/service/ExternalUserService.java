package com.zhjw.service;

import com.zhjw.entity.ExternalUser;

/**
 * Created by zhjw on 2023/3/6
 * <p>
 *
 * </p>
 *
 * @author zhjw
 * @date 2023/3/6
 */
public interface ExternalUserService {

    /**
     * 插入
     *
     * @param externalUser 插入实体
     * @return
     */
    void insert(ExternalUser externalUser);

    /**
     * 事务插入
     *
     * @param externalUser 插入实体
     * @return
     */
    void unionTransactional(ExternalUser externalUser);
}
