package com.zhjw.common.response;

/**
 * 响应编码枚举
 *
 * @author zhjw
 */
public enum ResponseCode {

    //操作成功
    SUCCESS("200", "操作成功"),
    //系统异常
    ERROR("500", "系统异常");

    private String code;
    private String msg;

    ResponseCode(String code, String msg) {

        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

}
