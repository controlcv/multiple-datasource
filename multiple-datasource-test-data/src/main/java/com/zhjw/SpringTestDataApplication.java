package com.zhjw;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;


@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@Slf4j
public class SpringTestDataApplication {
   
   /**
    * <p>
    *     springboot项目启动类
    * </p>
    *
    * @author zhjw
    * @param args
    */
    public static void main(String[] args) {
        SpringApplication.run(SpringTestDataApplication.class);
    }

}
