package com.zhjw.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 
 * @TableName t_master_user
 */
@Data
public class MasterUser implements Serializable {
    /**
     * 主键
     */
    private Long id;

    /**
     * 姓名
     */
    private String name;

    private static final long serialVersionUID = 1L;
}