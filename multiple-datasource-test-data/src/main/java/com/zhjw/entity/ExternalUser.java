package com.zhjw.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 
 * @TableName t_external_user
 */
@Data
public class ExternalUser implements Serializable {
    /**
     * 主键
     */
    private Long id;

    /**
     * 姓名
     */
    private String name;

    private static final long serialVersionUID = 1L;
}